/*
 * Copyright 2018  Christian Fiedler
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package conf

import "bufio"
import "io"
import "reflect"
import "strconv"
import "strings"

type Error struct{
	Element string
	Type string
	Expression string
	TypeUnsupported bool
	IllegalExpression bool
	SyntaxError bool
}

func (e Error) Error() string {
	if e.TypeUnsupported {
		return "unsupported type "+e.Type+" of field "+e.Element+" in struct"
	}else if e.IllegalExpression {
		return "illegal expression '"+e.Expression+"' in config file for field "+e.Element
	}else if e.SyntaxError {
		return "syntax error"
	}else{
		return "unknown error"
	}
}

/* This function reads configuration parameters from the
 * io.ReadCloser to the interface given which may be either a
 * map[string]string or a struct. Remember to pass a pointer type.
 */
func Read(f io.ReadCloser, v interface{}) error {
	defer f.Close()

	r := bufio.NewReader(f)
	m := map[string]string{}

	for {
		line,err := r.ReadString('\n')
		if err != nil {
			if err != io.EOF {
				return err
			}
			break
		}
		line = strings.TrimSpace(line)
		s1 := strings.IndexRune(line,' ')
		s2 := strings.IndexRune(line,'\t')
		if s2 >= 0 && (s2 < s1 || s1 < 0) {
			s1 = s2
		}

		// ignore empty lines and comments
		if len(line) == 0 || line[0] == '#' {
			continue
		}
		if s1 < 0 {
			return &Error{ SyntaxError: true }
		}

		name := line[:s1]
		val := strings.TrimSpace(line[s1:])
		m[name] = val
	}

	val := reflect.ValueOf(v)
	if val.Type() == reflect.TypeOf(&map[string]string{}) {
		// if v is of type map[string]string we can assign
		// m to it right away and return
		dst := reflect.Indirect(val)
		dst.Set(reflect.ValueOf(m))
		return nil
	}

	// otherwise assume a struct
	val = val.Elem()
	for i := 0; i < val.NumField(); i++ {
		valueField := val.Field(i)
		typeField := val.Type().Field(i)
		//tag := typeField.Tag
		typ := valueField.Type().Name()

		if x,found := m[typeField.Name]; found {
			dst := reflect.Indirect(valueField)

			// find the type of the struct's field
			// we support: bool, int, string and []string
			t := valueField.Type()
			if t == reflect.TypeOf(false) {
				if x == "yes" || x == "true" {
					dst.Set(reflect.ValueOf(true))
				}else if x == "no" || x == "false" {
					dst.Set(reflect.ValueOf(false))
				}else{
					return &Error{ Type: typ,
						Element: typeField.Name,
						IllegalExpression: true }
				}
			}else if t == reflect.TypeOf(0) {
				i,err := strconv.Atoi(x)
				if err != nil {
					return &Error{ Type: typ,
						Element: typeField.Name,
						IllegalExpression: true }
				}
				dst.Set(reflect.ValueOf(i))
			}else if t == reflect.TypeOf("") {
				// TODO: remove surrounding ""
				dst.Set(reflect.ValueOf(x))
			}else if t == reflect.TypeOf([]string{}){
				// TODO: correctly handle and remove ""
				dst.Set(reflect.ValueOf(strings.Split(x," ")))
			}else{
				return &Error{ Type: typ,
					Element: typeField.Name,
					TypeUnsupported: true }
			}
		}

		/*
		fmt.Printf("Field Name: %s,\t Field Value: %v,\t Tag Value: %s,\t Type: %s\n", typeField.Name, valueField.Interface(), tag.Get("tag_name"), valueField.Type().Name())
		if valueField.Type() == reflect.TypeOf(false) {
			fmt.Printf("bool\n")
		}
		*/
	}
	return nil
}

