# go-conffile

MIT-licensed module for config file parsing in Go.
It's very easy to use.

See below the config file syntax and usage of this module.

## syntax

The config file syntax is very similar to that used in sshd_config, e.g.:

```
# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

Port 22
PasswordAuthentication yes
UseDNS false


#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
ChrootDirectory "/home"


# Allow client to pass locale environment variables
AcceptEnv HOME DISPLAY

```

Lines starting with hashes are comments.
Otherwise the line must start with an identifier which is followed by its value.
Identifier and value are seperated by tabs and/or spaces.

Values can be of type **bool**, **int**, **string** and **[]string**
In the example above you can see that for *Port* int would be a reasonable type.
For *UseDNS* and *PasswordAuthentication* we could use a bool. *ChrootDirectory*
looks like a string and *AcceptEnv* fits best in a string slice.


## how to use it


Use the read function from the module

```
/* This function reads configuration parameters from the
 * io.ReadCloser to the interface given which may be either a
 * map[string]string or a struct. Remember to pass a pointer type.
 */
func Read(f io.ReadCloser, v interface{}) error {
         ...
}
```

to either parse your configuration as a map[string]string (obviously all the values of the parsed variables will be of type string and you will have to convert them yourself to the desired types), or parse the configuration as a struct dedicated for that task:

```
package main

import "bitbucket.org/chtisgit/go-conffile"
import "io"
import "os"
import "fmt"

type Config struct{
	UseDNS bool
	ChrootDirectory string
	PasswordAuthentication bool
	X11DisplayOffset int
	Port int
	AcceptEnv []string
}

func main(){
	f,err := os.Open("test.conf")
	if err != nil {
		fmt.Println("error: could not open test.conf!")
		return
	}

	/* read config in map[string]string */
	m := map[string]string{}
	conf.Read(f, &m)
	fmt.Printf("map:\n")
	for i,v := range m {
		fmt.Printf("- %s   %s\n",i,v)
	}

	/* read config in struct */
	f,err = os.Open("test.conf")
	c := Config{}
	conf.Read(f, &c)
	fmt.Printf("struct:\n")
	fmt.Printf("- UseDNS   %v\n",c.UseDNS)
	fmt.Printf("- ChrootDirectory %v\n",c.ChrootDirectory)
	fmt.Printf("- X11DisplayOffset   %v\n",c.X11DisplayOffset)
	fmt.Printf("- Port %v\n",c.Port)
	fmt.Printf("- AcceptEnv %v\n",c.AcceptEnv)
}

```

